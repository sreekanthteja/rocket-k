FROM scratch

WORKDIR /rocket-k

# INSTALL DEPENDENCIES
# COPY requirements.txt .
# RUN pip install -r requirements.txt
# COPY SRC TO DEST
COPY . /rocket-k

# RUN THE APP

CMD ["python", "rtsp.py"]

