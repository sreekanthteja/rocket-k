import subprocess
import threading
import sys, os, time
class KinesisProduce:
    def start_rtsp_client(self):
        print("===============starting==============")
        p1 = subprocess.run(["bash rtsp_client.sh"],shell=True)
    def start_rtsp_server(self):
        p1 = subprocess.run(["bash rtsp_server.sh"],shell=True)
if __name__ == "__main__":
    kinesis_produce = KinesisProduce()
    try:
        thread1 = threading.Thread(target=kinesis_produce.start_rtsp_client)
        thread2 = threading.Thread(target=kinesis_produce.start_rtsp_server)
        thread1.start()
        time.sleep(0.01)
        thread2.start()
    except KeyboardInterrupt:
        print('KeyboardInterrupt has occured')
        

